package com.example.reactdemo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@Slf4j
public class DemoController {
    @GetMapping("/recent")
    public Flux<String> getDemoString() throws InterruptedException {
        log.info("hello1");
        return getFluxStrings();
    }

    private Flux<String> getFluxStrings() {
        log.info("getFluxStrings");
        return Flux.range(0, 1000).map(String::valueOf);
    }

}
