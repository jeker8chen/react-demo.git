package com.example.reactdemo;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.test.StepVerifier;
import reactor.util.function.Tuple2;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReactTest {


    @Test
    public void 创建一个Flux并且输出() {
        Flux<String> flux = Flux.just("1", "2", "3", "4", "5");
        flux.subscribe(f -> System.out.println("Here's some number: " + f));
    }


    @Test
    public void 创建一个Flux并且断言输出() {
        Flux<String> flux = Flux.just("1", "2", "3", "4", "5");
        StepVerifier.create(flux)
                .expectNext("1")
                .expectNext("2")
                .expectNext("3")
                .expectNext("4")
                .expectNext("5")
                .verifyComplete();
    }

    @Test
    public void 从数组中创建一个集合() {
        String[] strs = {"1", "2", "3"};
        Flux<String> flux = Flux.fromArray(strs);
        StepVerifier.create(flux)
                .expectNext("1")
                .expectNext("2")
                .expectNext("3")
                .verifyComplete();

        List<String> strList = new ArrayList<>();
        strList.add("1");
        strList.add("2");
        strList.add("3");
        Flux.fromIterable(strList);

        Flux.fromStream(strList.stream());
    }

    @Test
    public void 提供范围生成一个Flux() {
        Flux<Integer> flux = Flux.range(0, 3);
        StepVerifier.create(flux)
                .expectNext(0)
                .expectNext(1)
                .expectNext(2)
                .verifyComplete();

        Flux<Long> flux1 = Flux.interval(Duration.ofSeconds(1L)).take(3L);
        StepVerifier.create(flux1)
                .expectNext(0L)
                .expectNext(1L)
                .expectNext(2L)
                .verifyComplete();
    }

    @Test
    public void 合并多个Flux() {
        Flux<Integer> flux = Flux.range(0, 3).delayElements(Duration.ofMillis(500));
        Flux<Integer> flux1 = Flux.range(3, 2).delaySubscription(Duration.ofMillis(250)).delayElements(Duration.ofMillis(500));
        Flux<Integer> flux2 = flux.mergeWith(flux1);
        flux2.subscribe(f -> System.out.println("Here's some number: " + f));
        StepVerifier.create(flux2)
                .expectNext(0)
                .expectNext(3)
                .expectNext(1)
                .expectNext(4)
                .expectNext(2)
                .verifyComplete();

        Flux<Tuple2<Integer, Integer>> flux3 = Flux.zip(flux, flux1);
        flux3.take(3).subscribe(f -> System.out.println(f.toString()));
        StepVerifier.create(flux3)
                .expectNextMatches(t -> t.getT1() == 0 && t.getT2() == 3)
                .expectNextMatches(t -> t.getT1() == 1 && t.getT2() == 4)
                .verifyComplete();

        Flux<Integer> flux4 = Flux.zip(flux, flux1, (x, y) -> x + y);
        flux4.take(3).subscribe(f -> System.out.println(f.toString()));
        StepVerifier.create(flux4)
                .expectNext(3)
                .expectNext(5)
                .verifyComplete();
    }

    @Test
    public void 只获取最先发布的Flux() {
        Flux<Integer> flux = Flux.range(0, 3).delayElements(Duration.ofMillis(500));
        Flux<Integer> flux1 = Flux.range(3, 2).delaySubscription(Duration.ofMillis(250)).delayElements(Duration.ofMillis(500));
        Flux<Integer> flux2 = Flux.first(flux, flux1);
        StepVerifier.create(flux2)
                .expectNext(0)
                .expectNext(1)
                .expectNext(2)
                .verifyComplete();
    }

    @Test
    public void 过滤Flux中的数据() {
        Flux<Integer> flux = Flux.range(0, 10).skip(8);
        StepVerifier.create(flux)
                .expectNext(8)
                .expectNext(9)
                .verifyComplete();

        Flux<Integer> flux1 = Flux.range(0, 10).delayElements(Duration.ofMillis(100))
                .skip(Duration.ofMillis(800));
        StepVerifier.create(flux1)
                .expectNext(7)
                .expectNext(8)
                .expectNext(9)
                .verifyComplete();

        Flux<Integer> flux2 = Flux.range(0, 10).delayElements(Duration.ofMillis(100)).take(Duration.ofMillis(350));
        StepVerifier.create(flux2)
                .expectNext(0)
                .expectNext(1)
                .expectNext(2)
                .verifyComplete();

        Flux<Integer> flux3 = Flux.range(0, 10).filter(n -> n < 2);
        StepVerifier.create(flux3)
                .expectNext(0)
                .expectNext(1)
                .verifyComplete();

        Flux<Integer> flux4 = Flux.just(1, 2, 3, 3, 4, 5, 5).distinct();
        StepVerifier.create(flux4)
                .expectNext(1)
                .expectNext(2)
                .expectNext(3)
                .expectNext(4)
                .expectNext(5)
                .verifyComplete();
    }

    @Test
    public void 映射Flux() {
        Flux<Integer> flux = Flux.just("1", "2", "3")
                .map(Integer::valueOf);
        StepVerifier.create(flux)
                .expectNext(1)
                .expectNext(2)
                .expectNext(3)
                .verifyComplete();

        Flux<Integer> flux1 = Flux.just("1", "2", "3", "4")
                .flatMap(m -> Mono.just(m).map(c -> Integer.valueOf(c)).subscribeOn(Schedulers.parallel()));
        List<Integer> list = Stream.of(1, 2, 3, 4).collect(Collectors.toList());
        StepVerifier.create(flux1)
                .expectNextMatches(list::contains)
                .expectNextMatches(list::contains)
                .expectNextMatches(list::contains)
                .expectNextMatches(list::contains)
                .verifyComplete();
    }

    @Test
    public void 缓冲Flux() {
        Flux<Integer> flux = Flux.just(1, 2, 3, 4, 5, 6);
        Flux<List<Integer>> listFlux = flux.buffer(3);
        StepVerifier.create(listFlux)
                .expectNext(Arrays.asList(1, 2, 3))
                .expectNext(Arrays.asList(4, 5, 6))
                .verifyComplete();

        Flux.just("apple", "orange", "banana", "kiwi", "strawberry")
                .buffer(3)
                .flatMap(x ->
                        Flux.fromIterable(x)
                                .map(y -> y.toUpperCase())
                                .subscribeOn(Schedulers.parallel())
                                .log()
                ).subscribe();

        Flux<Integer> flux1 = Flux.range(1, 6);
        Mono<List<Integer>> mono2 = flux1.collectList();
        StepVerifier.create(mono2)
                .expectNext(Arrays.asList(1, 2, 3, 4, 5, 6))
                .verifyComplete();

        Flux<Integer> flux2 = Flux.range(1, 6);
        Mono<Map<Object, Integer>> mapMono = flux2.collectMap(f -> String.valueOf(f + "i"));
        StepVerifier.create(mapMono)
                .expectNextMatches(m -> m.size() == 6
                        && m.get("1i").equals(1)
                        && m.get("2i").equals(2)
                        && m.get("3i").equals(3))
                .verifyComplete();
    }

    @Test
    public void Flux的逻辑操作() {
        Flux<Integer> flux = Flux.just(1, 2, 3, 4, 5, 6);
        Mono<Boolean> mono = flux.any(f -> f < 0);
        StepVerifier.create(mono)
                .expectNext(false)
                .verifyComplete();

        Mono<Boolean> mono1 = flux.all(f -> f > 0);
        StepVerifier.create(mono1)
                .expectNext(true)
                .verifyComplete();
    }
}
